require 'rails_helper'

RSpec.describe Customer, type: :model do
  it { is_expected.to have_db_column(:email) }
  it { is_expected.to have_db_column(:username) }
  it { is_expected.to have_db_column(:account) }
  it { is_expected.to have_db_column(:message) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:username) }
  it { is_expected.to validate_presence_of(:account) }
  it { is_expected.to validate_presence_of(:message) }
  it { is_expected.to validate_presence_of(:password) }
end
