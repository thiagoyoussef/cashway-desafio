require 'rails_helper'

feature 'new customer' do
  let(:admin_email) { Faker::Internet.email }
  let(:admin_password) { Faker::Crypto.md5 }

  before do
    Admin.create!(email: admin_email, password: admin_password)
    visit root_path
    click_on 'Login'
  end

  context 'when a valid admin try to login' do
    before do
      find('#admin_email').click
      find('#admin_email').set(admin_email)
      find('#admin_password').click
      find('#admin_password').set(admin_password)
      click_on 'Login'
    end

    it 'is able to login successfull' do
      expect(page).to have_text('Você deseja cadastrar um novo cliente?')
    end

    context 'when a new custumer is created successfully' do
      let(:created_customer) { Customer.last }
      let(:email) { Faker::Internet.email }
      let(:username) { Faker::TvShows::GameOfThrones.character }
      let(:account) { Faker::IDNumber.brazilian_citizen_number }
      let(:message) { Faker::TvShows::GameOfThrones.quote }
      let(:password) { Faker::Crypto.md5 }

      before do
        click_on 'Cadastrar cliente'
        find('#customer_email').click
        find('#customer_email').set(email)
        find('#customer_account').click
        find('#customer_account').set(account)
        find('#customer_username').click
        find('#customer_username').set(username)
        find('#customer_message').click
        find('#customer_message').set(message)
        find('#customer_password').click
        find('#customer_password').set(password)
        find('#customer_password_confirmation').click
        find('#customer_password_confirmation').set(password)
        click_on 'Cadastrar'
      end

      it 'redirects to show customer' do
        expect(page).to have_current_path("/customers/#{created_customer.id}")
      end

      it 'creates a new customer with given username' do
        expect(created_customer.username).to eq(username)
      end

      it 'creates a new customer with given email' do
        expect(created_customer.email).to eq(email)
      end

      it 'creates a new customer with given account' do
        expect(created_customer.account).to eq(account)
      end

      it 'creates a new customer with given message' do
        expect(created_customer.message).to eq(message)
      end
    end
  end

  context 'when an invalid admin try to login' do
    before do
      find('#admin_email').click
      find('#admin_email').set(admin_email)
      find('#admin_password').click
      find('#admin_password').set(Faker::TvShows::GameOfThrones.character)
      click_on 'Login'
    end

    it 'is not able to login successfull' do
      expect(page).to have_current_path('/admins/sign_in')
    end
  end
end
