Rails.application.routes.draw do
  devise_for :admins, only: :sessions
  devise_for :customers, only: :registrations, controllers: { registrations: 'customers/registrations' }

  root 'home#index'
  resources :customers, only: :show
end
