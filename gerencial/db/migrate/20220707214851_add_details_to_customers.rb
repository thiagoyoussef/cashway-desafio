class AddDetailsToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :username, :string
    add_column :customers, :account, :string
    add_column :customers, :message, :string
  end
end
