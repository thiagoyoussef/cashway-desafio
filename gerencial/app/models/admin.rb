class Admin < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable
  validates_presence_of :email, :password
end
