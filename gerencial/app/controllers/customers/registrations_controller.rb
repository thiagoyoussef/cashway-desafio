# frozen_string_literal: true

class Customers::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]

  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      redirect_to customer_path(@customer)
    else
      redirect_to new_customer_registration_path, alert: 'Ops, algo deu errado!'
    end
  end

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[username account message])
  end

  def customer_params
    params.require(:customer).permit(:username, :account, :email, :message, :password, :password_confirmation)
  end
end
