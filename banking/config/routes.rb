Rails.application.routes.draw do
  devise_for :customers, only: :sessions

  root to: 'customers#show'
end
