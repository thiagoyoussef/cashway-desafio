class Customer < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates_uniqueness_of :email, :username, :account
  validates_presence_of :email, :username, :account, :message, :password
end
