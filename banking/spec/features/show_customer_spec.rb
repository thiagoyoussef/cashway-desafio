require 'rails_helper'

feature 'show customer' do
  let(:account) { Faker::IDNumber.brazilian_citizen_number }
  let(:password) { Faker::Crypto.md5 }
  let(:message) { Faker::TvShows::GameOfThrones.quote }

  before do
    Customer.create!(
      account: account, message: message,
      username: Faker::TvShows::GameOfThrones.character,
      email: Faker::Internet.email, password: password
    )
    visit root_path
  end

  context 'when a valid customer try to login' do
    before do
      find('#customer_account').click
      find('#customer_account').set(account)
      find('#customer_password').click
      find('#customer_password').set(password)
      click_on 'Login'
    end

    it 'shows customer message' do
      expect(page).to have_text("Mensagem do gerente: #{message}")
    end
  end

  context 'when an invalid admin try to login' do
    before do
      find('#customer_account').click
      find('#customer_account').set(account)
      find('#customer_password').click
      find('#customer_password').set(Faker::TvShows::GameOfThrones.character)
      click_on 'Login'
    end

    it 'is not able to login successfull' do
      expect(page).to have_current_path('/customers/sign_in')
    end
  end
end
