# cashway-desafio


## Setup

```
git clone https://gitlab.com/thiagoyoussef/cashway-desafio.git
cd gerencial
bundle install
rails db:migrate
rails db:seed
rails server
cd ../banking
bundle install
rails db:migrate
rails db:seed
rails server -p 3001
```

## Seed

Usuário admin no app Gerencial:

email `admin@gerencial.com.br`  
senha `123123`

Usuário cliente no app Banking(Opcional):

nome `thiago`  
email `cliente@banking.com.br`  
conta `12345`  
mensagem `Olá, seja bem vindo! Qualquer dúvida entre em contato.`  
senha `123123`

## Acessar apps

Endereço Gerencial: `localhost:3000`  
Endereço Banking: `localhost:3001`

## Rodar os testes

```
bundle exec rspec
```
